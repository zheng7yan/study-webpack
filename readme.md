- 使用`npx build`命令会生成`dist/main.js`

    `npm run build`也可以

- `assets management`

    - `css file`

        `npm install --save-dev style-loader css-loader`

        ```
        +   module: {
        +     rules: [
        +       {
        +         test: /\.css$/,
        +         use: [
        +           'style-loader',
        +           'css-loader'
        +         ]
        +       }
        +     ]
        +   }
        ```

- `load images`

    `npm install --save-dev file-loader`

    ```
    +       {
    +         test: /\.(png|svg|jpg|gif)$/,
    +         use: [
    +           'file-loader'
    +         ]
    +       }
    ```

- `load data`

    `npm install --save-dev csv-loader xml-loader`

- `html-webpack-plugin` will generate its own index.html in `dist` folder

    `npm install --save-dev html-webpack-plugin`

- `clean-webpack-plugin`

    `npm install --save-dev clean-webpack-plugin`