import _ from 'lodash';
import './style.css';
import icon from './static/image/1976865.png';
import data from './static/data/data.xml';
import printMe from './static/js/print.js';

function component() {
  const element = document.createElement('div');

  // Lodash, currently included via a script, is required for this line to work
  element.innerHTML = _.join(['Hello', 'webpack'], ' ');
  element.classList.add('hello');

  const myIcon = new Image();
  myIcon.src = icon;
  element.appendChild(myIcon);

  console.log(data);

  const  btn = document.createElement('button');
  btn.innerHTML = 'click me and check the console';
  btn.onclick = printMe;
  element.appendChild(btn);

  return element;
}

document.body.appendChild(component());